#coding: utf-8
import sys
 
n=int(sys.argv[1])
 
if len(sys.argv) == 3 and sys.argv[2] == "-v":
    show = True
elif(len(sys.argv) != 3 and len(sys.argv) != 2):
	print "Error!"
	show = False
else:
	show = False
	
if (n<0):
	print "Error!"
	sys.exit(1)
 
class Hanoi:
	def __init__(self, n): #����������� �������������
		self.a = []
		for i in range(n):
			self.a.append(n - i)#���������� ������ � ������� ��������
		self.b = []
		self.c = []
		self.steps=0
		if (show):
			self.display(self.a,self.b,self.c)#����� ������
			
	
	def display(self,a,b,c):#������� ������
		
		print("")
		print "\t1:", "\t",self.a
		print("")
		print "\t2:", "\t",self.b
		print("")
		print "\t3:", "\t",self.c
		print("")
	
	def move(self, a, b, c, n): #������� ��������
		if n == 1:
			c.append(a[len(a)-1]) #���������� ����� �� �������� �
			del a[len(a)-1] #�������� ����� �� �������� �
			self.steps+=1
			if (show):
				self.display(self.a,self.b,self.c)
		else:
			self.move(a, c, b, n - 1)#�������������� ���� ������ �� ������� � ����� ���������� �� �������� b
			self.move(a, b, c, 1)#�������������� ������� ����� �� �������� �
			self.move(b, a, c, n - 1)#�������������� �� ������� b �� �������� � ���������� ������
		
	def Solve(self):	
		self.move(self.a,self.b,self.c,n)
		print ('Number of steps= ', self.steps)
		
x = Hanoi(n)
x.Solve()
 
sys.exit(u"Complete")